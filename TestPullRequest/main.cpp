#include <iostream>
using namespace std;

int getSum(int from, int to)
{
	return (from + to) * (to - from + 1) >> 1;
}

int getMul(int from, int to)
{
	int mul = 1;

	for (int i = from; i < to; i++)
		mul *= i;

	return mul;
}

int main()
{
	cout << getMul(1, 5) << endl;
}